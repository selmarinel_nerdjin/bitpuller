<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserRegistration;
use App\Repository\User\Orm\UserRepository;
use App\Repository\UserRegistrationRepository;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    public function __construct(TokenStorageInterface $tokenStorage,
                                AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function register(Request $request, \Swift_Mailer $swiftMailer)
    {
        $repository = $this->get(UserRepository::class);
        $user = $repository->getUserFromHttpRequest($request);
        $validator = $this->get("validator");
        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return $this->json(['errors' => $errors], 401);
        }
        $repository->save($user);

        $message = (new \Swift_Message("Welcome to Tetra Crypt"))
            ->setFrom("tetra_crypt@gmail.com")
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView("mails/welcome.html.twig",
                    [
                        "name" => $user->getDetails()->getFirstName() . " " . $user->getDetails()->getLastName(),
                        "hash" => $user->getRegistration()->getToken()
                    ]
                ),
                "text/html"
            );
        $swiftMailer->send($message);

        return $this->json(["message" => "Success"]);
    }

    public function activeUser(Request $request, UserRegistrationRepository $registrationRepository)
    {
        /** @var UserRegistration $token */
        $token = $registrationRepository->findByToken($request->attributes->get("hash"));
        if ($token) {
            $token->setActive(1);
            $registrationRepository->save($token);
            $user = $token->getUser();
            $providerKey = 'main'; // your firewall name
            $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
            $authenticatedToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authenticatedToken);
            return $this->redirectToRoute("index");
        }
        return $this->redirectToRoute("index");
    }
}
