<?php

namespace App\Controller;

use App\Entity\File;
use App\Entity\User;
use App\Repository\User\Orm\UserRepository;
use App\Repository\User\UserRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserApiController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    public function __construct(UserRepositoryInterface $userRepository,
                                TokenStorageInterface $tokenStorage,
                                AuthenticationManagerInterface $authenticationManager)
    {
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    public function create(Request $request, \Swift_Mailer $swiftMailer)
    {
        $repository = $this->get(UserRepository::class);
        $user = $repository->getUserFromHttpRequest($request);
        $validator = $this->get("validator");
        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return $this->render('blocks/validation.html.twig', [
                'errors' => $errors,
            ]);
        }
        $repository->save($user);

        $message = (new \Swift_Message("Welcome to Tetra Crypt"))
            ->setFrom("tetra_crypt@gmail.com")
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView("mails/welcome.html.twig",
                    [
                        "name" => $user->getDetails()->getFirstName() . " " . $user->getDetails()->getLastName(),
                        "hash" => $user->getRegistration()->getToken()
                    ]
                ),
                "text/html"
            );
        $swiftMailer->send($message);


        return $this->redirectToRoute('index');
    }

    public function edit(Request $request, UserInterface $user = null)
    {
        $repository = $this->get(UserRepository::class);
        if (!$user) {
            return $this->redirectToRoute("login");
        }
        if ($user instanceof User) {
            $repository->updateUserFromHttpRequest($user, $request);
            $repository->save($user);
        }

//        $file = $request->files->get("file");
//        $fileName = md5(uniqid()).'.'.$file->guessExtension();
//        $file->move(
//            "d:/tmp/files",
//            $fileName
//        );
//
//        $em = $this->getDoctrine()->getManager();
//        $fileEntity = new File();
//        $fileEntity->setName($fileName);
//        $fileEntity->setPath($file->getPath());
//        $em->persist($fileEntity);
//        $em->flush();


        return $this->redirectToRoute("me");
    }
}
