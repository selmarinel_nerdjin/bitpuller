<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\User\Orm\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends Controller
{
    public function index(UserInterface $user = null)
    {
        if (!$user) {
            return $this->render("index.html.twig");
        }
        /** @var User $me */
        $me = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($user->getUsername());
        return $this->render("user/user.html.twig", ["user" => $me]);
    }

    public function viewCreate()
    {
        return $this->render("user/create.html.twig");
    }

    public function editAction(UserInterface $user = null)
    {
        if (!$user) {
            return $this->redirectToRoute("login");
        }
        return $this->render("user/edit.html.twig", ["user" => $user]);
    }


}
