<?php

namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatorInterface;

class MainController extends Controller
{
    public function index(Request $request, TranslatorInterface $translator)
    {
        $translator->setLocale($request->getLocale());
        return $this->render("index.html.twig");
    }

    public function localeIndex(Request $request)
    {
        $request->getLocale();
        return $this->redirectToRoute("index");

    }

    public function send(UserInterface $user = null)
    {
        if ($user) {
            $me = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($user->getUsername());
            $mailExecutable = $this->get("mail.executable");
            $mailExecutable->sendWelcomeForUser($me);
        }
        return $this->redirectToRoute("me");
    }
}