<?php

namespace App\Services;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    public function __construct()
    {
    }

    public function save(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            "d:/tmp/files",
            $fileName
        );
    }
}