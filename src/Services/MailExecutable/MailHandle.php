<?php

namespace App\Services\MailExecutable;


use App\Entity\User;

class MailHandle
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig_Environment
     */
    private $environment;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $environment)
    {
        $this->mailer = $mailer;
        $this->environment = $environment;
    }

    public function sendWelcomeForUser(User $user): void
    {

        try {
            $template = $this->environment->render('mails/welcome.html.twig', ['name' => $user->getDetails()->getFirstName()]);
        } catch (\Twig_Error_Loader $e) {
        } catch (\Twig_Error_Runtime $e) {
        } catch (\Twig_Error_Syntax $e) {
        }

        $message = (new \Swift_Message("Welcome to PitPuller"))
            ->setFrom("no-reply.bitpuller@gmail.com")
            ->setTo($user->getEmail())
            ->setBody($template,
                'text/html');
//        $this->mailer->createMessage($message);
        $this->mailer->send($message);
    }
}