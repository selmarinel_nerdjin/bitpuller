<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class UserDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "Your first name must be at least {{ limit }} characters long",
     *     maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100)
     */
    private $first_name;

    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "Your last name must be at least {{ limit }} characters long",
     *     maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100)
     */
    private $last_name;

    /**
     * @Assert\Length(
     *     min = 5,
     *     max = 50,
     *     minMessage = "Your cell must be at least {{ limit }} characters long",
     *     maxMessage = "Your cell cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=50)
     */
    private $cell;

    /**
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg", "image/png"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    public function setFile(File $file)
    {
        $this->file = $file;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * @param mixed $cell
     */
    public function setCell($cell): void
    {
        $this->cell = $cell;
    }


}
