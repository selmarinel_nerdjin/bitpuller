<?php

namespace App\Repository;

use App\Entity\UserRegistration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRegistrationRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, UserRegistration::class);
        $this->entityManager = $entityManager;
    }

    public function findByToken($token)
    {
        return $this->findOneBy(["token" => $token]);
    }

    public function findUserByToken($token)
    {
        /** @var UserRegistration $entity */
        $entity = $this->findOneBy(["token" => $token, "active" => 0]);
        if ($entity) {
            return $entity->getUser();
        }
        return null;
    }

    public function save(UserRegistration $userRegistration): void
    {
        $this->entityManager->persist($userRegistration);
        $this->entityManager->flush();
    }
}
