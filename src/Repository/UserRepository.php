<?php

namespace App\Repository;

use App\Entity\File;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }


    public function loadUserByUsername($username)
    {
        $user = $this->createQueryBuilder("u")
            ->innerJoin("u.registration", 'r')
            ->where("u.email = :username")
            ->andWhere("r.active = 1")
            ->setParameter("username", $username)
            ->getQuery();
        return $user->getOneOrNullResult();
    }
}
