<?php

namespace App\Repository\User\Orm;

use App\Entity\File;
use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\UserRegistration;
use App\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class UserRepository implements UserRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(User::class);
    }

    public function getOneById(int $id): ?User
    {
        return null;
    }

    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param Request $request
     * @return User
     */
    public function getUserFromHttpRequest(Request $request): User
    {
        $user = new User();
        $user->setEmail($request->get('email'));
        $encoded = new BCryptPasswordEncoder(10);
        $user->setPassword($encoded->encodePassword($request->get("password"), ''));
        $userDetails = new UserDetails();
        $userDetails->setFirstName($request->get("first_name"));
        $userDetails->setLastName($request->get("last_name"));
        $userDetails->setCell($request->get('cell'));
        $user->setDetails($userDetails);
        $userRegistration = new UserRegistration();
        $userRegistration->setUser($user);
        $userRegistration->setActive(0);
        $userRegistration->setToken(md5($user->getEmail() . time() . rand(0, 100000)));
        $user->setRegistration($userRegistration);
        return $user;
    }

    /**
     * @param User $user
     * @param Request $request
     */
    public function updateUserFromHttpRequest(User $user, Request $request): void
    {
        if ($request->get("password")) {
            $encoded = new BCryptPasswordEncoder(10);
            $user->setPassword($encoded->encodePassword($request->get("password"), ''));
        }
        $userDetails = new UserDetails();
        $userDetails->setFirstName($request->get("first_name"));
        $userDetails->setLastName($request->get("last_name"));
        $userDetails->setCell($request->get('cell'));
//        /**
//         * @var UploadedFile $file
//         */
//        $file = $request->files->get("file");
//        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
//        $file->move(
//            __DIR__ . "/../../../../public/files/",
//            $fileName
//        );
//        $fileEntity = new File();
//        $fileEntity->setName($fileName);
//        $fileEntity->setPath(__DIR__ . "/../../../../public/files/");
//        $userDetails->setFile($fileEntity);
        $user->setDetails($userDetails);
    }

}