<?php

namespace App\Repository\User;

use App\Entity\User;

interface UserRepositoryInterface
{
    /**
     * @param int $id
     * @return User|null
     */
    public function getOneById(int $id): ?User;

    /**
     * @param User $user
     */
    public function save(User $user): void;
}