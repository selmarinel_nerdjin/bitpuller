Feature: Registration
  Given: I am ne user in system

  Scenario: Open register popup
    When: I on landing page
    And: I click on login link
    And: I switch registration
    Then: Login form change to register form

  Scenario: Register on popup
    When: I on register form
    And: Insert register data
    And: Click on register button
    Then: System send register email with registration code
    And: I see information on screen "Success"

  Scenario: Confirm register
    When: I make first registration step
    And: I open my email cabinet
    And: I open email message
    And: Click on link
    Then: Browser open in new tab bitpuller page
    And: I confirm my registration
    And: I see this information
