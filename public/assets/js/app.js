var socketUSD = new WebSocket("wss://api.bitfinex.com/ws/2");

var $usdInElement = $("#usdIn");
var $usdOutElement = $("#usdOut");
var $currency = $("#currency");
var $crypto = $("#crypto");
var $value = $("#value");
var $result = $("#result");

var curses = {
    usd: {
        in: 0,
        out: 0
    },
    eur: {
        in: 0,
        out: 0
    },
    xrp: {
        in: 0,
        out: 0
    },
    eth: {
        in: 0,
        out: 0
    },
    ltc: {
        in: 0,
        out: 0
    },
    bch: {
        in: 0,
        out: 0
    }
};

function calculate() {
    if ($crypto.val()) {
        var $const = (curses[$crypto.val()]) ? curses[$crypto.val()].in : 1;
        if (!curses[$crypto.val()]) {
            $result.val($value.val() * curses[$currency.val()].in.toFixed(4));
        } else {
            $result.val($value.val() * curses[$crypto.val()].in.toFixed(4));
        }
    }
}

socketUSD.onopen = function (event) {
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "BTCUSD"
    }));
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "BTCEUR"
    }));
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "ETHUSD"
    }));
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "LTCUSD"
    }));
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "BCHUSD"
    }));
    socketUSD.send(JSON.stringify({
        channel: "ticker",
        event: "subscribe",
        pair: "XRPUSD"
    }));
}

function revert(object) {
    if (object) {
        return {
            c: object.c,
            in: (object.in) ? 1 / (object.in) : 0,
            out: (object.out) ? 1 / (object.out) : 0,
        }
    }
    return {
        in: 0,
        out: 0
    };
}

var dataModel = {};
socketUSD.onmessage = function (event) {
    var data = JSON.parse(event.data);
    if (data.event === "subscribed") {
        dataModel[data.chanId] = {
            "in": 0,
            "out": 0,
            "c": data.pair
        }
    }
    if (dataModel[data[0]] && data[1][0] && typeof data[1][0] === "number") {
        dataModel[data[0]].in = data[1][0];
        dataModel[data[0]].out = data[1][2];
    }
    curses.usd = findInDataModel("BTCUSD");
    curses.eur = findInDataModel("BTCEUR");

    curses.xrp = findInDataModel("XRPUSD");
    curses.eth = findInDataModel("ETHUSD");
    curses.ltc = findInDataModel("LTCUSD");
    curses.bch = findInDataModel("BCHUSD");
    try {
        $usdInElement.text("$" + curses.usd.in);
        $usdOutElement.text("$" + curses.usd.out);
    } catch (exception) {
        console.warn(exception);
    }
    calculate();
};

function showEuroUnavailable() {
    $('#currency option[value=eur]').show();
}

function hideEuroUnavailable() {
    $('#currency option[value=eur]').hide();
}

function findInDataModel(what) {
    var found = {in: 0, out: 0};
    $.each(dataModel, function (k, v) {
        if (v.c === what) {
            found = v;
        }
    });
    return found;
}

$(document).ready(function () {
    var maxHeight = 0;
    $.each($("b.number-titled"), function (k, $el) {
        if ($($el).height() > maxHeight) {
            maxHeight = $($el).height();
        }
    });
    $("b.number-titled").css("height", maxHeight + 20);

    $currency.on("change", function () {
        calculate();
    });
    $crypto.on("change", function () {
        if ($(this).val() !== "btc") {
            hideEuroUnavailable();
            $('#currency option[value=usd]').prop("selected", true);
        } else {
            showEuroUnavailable()
        }
        calculate();
    });
    $value.on("keyup", function () {
        calculate();
    });
    $value.on("change", function () {
        calculate();
    });
});

$(window).bind('beforeunload',function(){
    socketUSD.close();
});

$( window ).unload(function() {
    socketUSD.close();
});