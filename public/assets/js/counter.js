function startCircable() {
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1500,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now) + "%");

            }
        });
    });
}

window.onscroll = function () {
    onScrollAction()
};
window.usedCircles = false;

function onScrollAction() {
    var top = $("#section-points").offset().top;
    if (document.documentElement.scrollTop > top && !window.usedCircles) {
        window.usedCircles = true;
        startCircable();
    }
}